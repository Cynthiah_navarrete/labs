from django import forms
from .models import Bandas_de_Rock

# Cliente
class Bandas_de_RockForm(forms.ModelForm):
    class Meta:
        model = Bandas_de_Rock
        fields = [
            'id',
            'Nombre_de_la_banda',
            'No_de_integrantes',
            'Fecha_de_creacion',
            'Ciudad_de_origen',
            'Subgenero_Principal',
        ]
        labels = {
                'id': 'Identificador',
                'Nombre_de_la_banda': 'Nombre de la banda',
                'No_de_integrantes':'Numero de integrantes',
                'Fecha_de_creacion':'Fecha de creacion',
                'Ciudad_de_origen':'Ciudad de origen',
                'Subgenero_Principal':'Subgenero Principal',
        }
