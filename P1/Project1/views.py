# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import Bandas_de_RockForm
from .models import Bandas_de_Rock



################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarBandas_de_Rock(CreateView):
    model = Bandas_de_Rock
    form_class = Bandas_de_RockForm
    template_name = 'Project1/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarBandas_de_Rock( ListView):
    model = Bandas_de_Rock
    template_name = 'Project1/Retrieve.html'
    context_object_name = 'bandas'
    queryset = Bandas_de_Rock.objects.all()


#DETAILEVIEW
class DetailBandas_de_Rock( DetailView):
    model = Bandas_de_Rock
    template_name = 'Project1/Detail.html'
    context_object_name = 'bandas'


#UPDATEVIEW
class EditarBandas_de_Rock( UpdateView):
    model = Bandas_de_Rock
    form_class = Bandas_de_RockForm
    template_name = 'Project1/Update.html'
    context_object_name = 'bandas'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarBandas_de_Rock( DeleteView):
    model = Bandas_de_Rock
    template_name = "Project1/Delete.html"
    context_object_name = "bandas"
    success_url = reverse_lazy('Retrieve')


class ACDC(TemplateView):
    template_name = 'Project1/ACDC.html'

class DEEPP(TemplateView):
    template_name = 'Project1/DEEPP.html'

class GUNS(TemplateView):
    template_name = 'Project1/GUNS.html'

class IRONM(TemplateView):
    template_name = 'Project1/IRONM.html'

class NIRVANA(TemplateView):
    template_name = 'Project1/NIRVANA.html'

class KISS(TemplateView):
    template_name = 'Project1/KISS.html'

class LEDZ(TemplateView):
    template_name = 'Project1/LEDZ.html'

class METALLICA(TemplateView):
    template_name = 'Project1/METALLICA.html'

class PINKF(TemplateView):
    template_name = 'Project1/PINKF.html'

class QUEEN(TemplateView):
    template_name = 'Project1/QUEEN.html'
