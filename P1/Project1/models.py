# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 1
#MODEL : BANDAS DE ROCK

class Bandas_de_Rock(models.Model):
    id = models.AutoField(primary_key = True)
    Nombre_de_la_banda = models.CharField(max_length = 20)
    No_de_integrantes = models.IntegerField()
    Fecha_de_creacion = models.IntegerField()
    Ciudad_de_origen = models.CharField(max_length = 20)
    Subgenero_Principal = models.CharField(max_length = 20)


    def __str__(self):
        return self.Nombre_de_la_banda
