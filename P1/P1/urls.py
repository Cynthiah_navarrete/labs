"""P1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project1 import views
from Project1.views import ListarBandas_de_Rock, AgregarBandas_de_Rock, EditarBandas_de_Rock, EliminarBandas_de_Rock, DetailBandas_de_Rock

#.views import ACDC, DEEPP, GUNS, IRONM, KISS, LEDZ, METALLICA, NIRVANA, QUEEN, PINKF

urlpatterns = [

    url(r'^admin/', admin.site.urls),

    #CRUD: RETRIEVEVIEW
    url(r'^$', ListarBandas_de_Rock.as_view(), name= 'Retrieve'),

    #CRUD: CREATEVIEW
    url(r'^Create/$', AgregarBandas_de_Rock.as_view(), name= 'Create'),

    #CRUD: DETAILEVIEW
    url(r'^Detail/(?P<pk>\d+)/$', DetailBandas_de_Rock.as_view(), name= 'Detail'),

    #CRUD:UPDATEVIEW
    url(r'^Update/(?P<pk>\d+)/$', EditarBandas_de_Rock.as_view(), name= 'Update'),

    #CRUD: DeleteView
    url(r'^Delete/(?P<pk>\d+)/$', EliminarBandas_de_Rock.as_view(), name= 'Delete'),


#   url(r'^$', ACDC.as_view(), name= 'ACDC'),
#   url(r'^DEEPP/$',DEEPP.as_view(),name= 'DEEPP'),
#   url(r'^GUNS/$', GUNS.as_view(), name= 'GUNS'),
#    url(r'^IRONM/$',  IRONM.as_view(),    name= 'IRONM'),
#    url(r'^KISS/$', KISS.as_view(), name= 'KISS'),
#    url(r'^LEDZ/$', LEDZ.as_view(), name= 'LEDZ'),
#    url(r'^METALLICA/$', METALLICA.as_view(), name= 'METALLICA'),
#    url(r'^NIRVANA/$',NIRVANA.as_view(), name= 'NIRVANA'),
#    url(r'^PINKF/$', PINKF.as_view(), name= 'PINKF'),
]
