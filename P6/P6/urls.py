"""P6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project6 import views
from Project6.views import AgregarPelicula, ListarPelicula, DetailPelicula,EditarPelicula, EliminarPelicula

urlpatterns = [
    url(r'^admin/', admin.site.urls),


        #CRUD: RETRIEVEVIEW
        url(r'^$', ListarPelicula.as_view(), name= 'Retrieve'),

        #CRUD: CREATEVIEW
        url(r'^Create/$', AgregarPelicula.as_view(), name= 'Create'),

        #CRUD: DETAILEVIEW
        url(r'^Detail/(?P<pk>\d+)/$', DetailPelicula.as_view(), name= 'Detail'),

        #CRUD:UPDATEVIEW
        url(r'^Update/(?P<pk>\d+)/$', EditarPelicula.as_view(), name= 'Update'),

        #CRUD: DeleteView
        url(r'^Delete/(?P<pk>\d+)/$', EliminarPelicula.as_view(), name= 'Delete'),

        
    #url(r'^$', Pelicula1.as_view(), name= 'Pelicula1'),
    #url(r'^Pelicula2/$',Pelicula2.as_view(),name= 'Pelicula2'),
    #url(r'^Pelicula3/$', Pelicula3.as_view(), name= 'Pelicula3'),
    #url(r'^Pelicula4/$',  Pelicula4.as_view(),    name= 'Pelicula4'),
    #url(r'^Pelicula5/$', Pelicula5.as_view(), name= 'Pelicula5'),
    #url(r'^Pelicula6/$', Pelicula6.as_view(), name= 'Pelicula6'),
    #url(r'^Pelicula7/$', Pelicula7.as_view(), name= 'Pelicula7'),
    #url(r'^Pelicula8/$',Pelicula8.as_view(), name= 'Pelicula8'),
    #url(r'^Pelicula9/$', Pelicula9.as_view(), name= 'Pelicula9'),
    #url(r'^Pelicula10/$', Pelicula10.as_view(), name= 'Pelicula10'),
]
