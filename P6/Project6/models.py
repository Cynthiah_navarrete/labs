# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 6
#MODEL : PELICULAS

class Pelicula(models.Model):
    id = models.AutoField(primary_key = True)
    Titulo = models.CharField(max_length = 50)
    Director= models.CharField(max_length = 50)
    Estreno = models.IntegerField()
    Genero = models.CharField(max_length = 50)


    def __str__(self):
        return self.Titulo
