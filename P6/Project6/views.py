# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import PeliculaForm
from .models import Pelicula

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarPelicula(CreateView):
    model = Pelicula
    form_class = PeliculaForm
    template_name = 'Project6/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarPelicula( ListView):
    model = Pelicula
    template_name = 'Project6/Retrieve.html'
    context_object_name = 'peliculas'
    queryset = Pelicula.objects.all()


#DETAILEVIEW
class DetailPelicula( DetailView):
    model = Pelicula
    template_name = 'Project6/Detail.html'
    context_object_name = 'peliculas'


#UPDATEVIEW
class EditarPelicula( UpdateView):
    model = Pelicula
    form_class = PeliculaForm
    template_name = 'Project6/Update.html'
    context_object_name = 'peliculas'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarPelicula( DeleteView):
    model = Pelicula
    template_name = "Project6/Delete.html"
    context_object_name = "peliculas"
    success_url = reverse_lazy('Retrieve')


# Create your views here.

class Pelicula1(TemplateView):
    template_name = 'Project6/1.html'

class Pelicula2(TemplateView):
    template_name = 'Project6/2.html'

class Pelicula3(TemplateView):
    template_name = 'Project6/3.html'

class Pelicula4(TemplateView):
    template_name = 'Project6/4.html'

class Pelicula5(TemplateView):
    template_name = 'Project6/5.html'

class Pelicula6(TemplateView):
    template_name = 'Project6/6.html'

class Pelicula7(TemplateView):
    template_name = 'Project6/7.html'

class Pelicula8(TemplateView):
    template_name = 'Project6/8.html'

class Pelicula9(TemplateView):
    template_name = 'Project6/9.html'

class Pelicula10(TemplateView):
    template_name = 'Project6/10.html'
