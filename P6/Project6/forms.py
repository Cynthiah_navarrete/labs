from django import forms
from .models import Pelicula

# Cliente
class PeliculaForm(forms.ModelForm):
    class Meta:
        model = Pelicula
        fields = [
            'id',
            'Titulo',
            'Director',
            'Estreno',
            'Genero',
        ]
        labels = {
                'id': 'Identificador',
                'Titulo': 'Titulo',
                'Director':'  Director',
                'Estreno':' Estreno ',
                'Genero':' Genero',
        }
