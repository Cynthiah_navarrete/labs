# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-04-22 06:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pelicula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Titulo', models.CharField(max_length=50)),
                ('Director', models.CharField(max_length=50)),
                ('Estreno', models.IntegerField()),
                ('Genero', models.CharField(max_length=50)),
            ],
        ),
    ]
