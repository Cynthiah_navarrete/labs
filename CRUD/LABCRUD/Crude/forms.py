from django import forms
from .models import SitemasOperativos

# Cliente
class SitemasOperativosForm(forms.ModelForm):
    class Meta:
        model = SitemasOperativos
        fields = [
            'id',
            'Nombre',
            'Desarrollador',
            'Tipo',
            'Lanzamiento',
            'Ultima_Version_Estable',
        ]
        labels = {
            'id': 'Identificador',
            'Nombre':'Nombre',
            'Desarrollador':'Desarrollador',
            'Tipo':'Tipo',
            'Lanzamiento' : 'Lanzamiento',
            'Ultima_Version_Estable':'Ultima Version Estable',
        }
