# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.

class SitemasOperativos(models.Model):
    id = models.AutoField(primary_key = True)
    Nombre = models.CharField(max_length = 100)
    Desarrollador = models.CharField(max_length = 100)
    TIPOS = (("PC", "Computadora Personal"),
    ("M", "Dispositivos Moviles"), ("R","Relojes Inteligentes" ))
    Tipo = models.CharField(max_length = 2, choices = TIPOS, default = "PC")
    Lanzamiento = models.DateField()
    Ultima_Version_Estable = models.CharField(max_length = 100)


    def __str__(self):
        return "{}".format(self.Nombre)
