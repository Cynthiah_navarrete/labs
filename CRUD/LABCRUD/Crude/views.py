# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .models import SitemasOperativos
from .forms import SitemasOperativosForm


# Create your views here.

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarSitemasOperativos(CreateView):
    model = SitemasOperativos
    form_class = SitemasOperativosForm
    template_name = 'TCrude/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarSitemasOperativos( ListView):
    model = SitemasOperativos
    template_name = 'TCrude/Retrieve.html'
    context_object_name = 'sistemas'
    queryset = SitemasOperativos.objects.all()


#DETAILEVIEW
class DetailSitemasOperativos( DetailView):
    model = SitemasOperativos
    template_name = 'TCrude/Detail.html'
    context_object_name = 'sistemas'


#UPDATEVIEW
class EditarSitemasOperativos( UpdateView):
    model = SitemasOperativos
    form_class = SitemasOperativosForm
    template_name = 'TCrude/Update.html'
    context_object_name = 'sistemas'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarSitemasOperativos( DeleteView):
    model = SitemasOperativos
    template_name = "TCrude/Delete.html"
    context_object_name = "sistemas"
    success_url = reverse_lazy('Retrieve')
