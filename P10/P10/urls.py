"""P10 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project10 import views
from Project10.views import ListarPais, AgregarPais, DetailPais,  EliminarPais, ActualizarPais
#Paises_Africanos1, Paises_Africanos2, Paises_Africanos3, Paises_Africanos4, Paises_Africanos5, Paises_Africanos6, Paises_Africanos7, Paises_Africanos8, Paises_Africanos9, Paises_Africanos10

urlpatterns = [
    url(r'^admin/', admin.site.urls),

     #CRUD: RETRIEVEVIEW
     url(r'^$', ListarPais.as_view(), name= 'Retrieve'),

     #CRUD: CREATEVIEW
     url(r'^Create/$', AgregarPais.as_view(), name= 'Create'),

     #CRUD: DETAILEVIEW
     url(r'^Detail/(?P<pk>\d+)/$', DetailPais.as_view(), name= 'Detail'),

     #CRUD:UPDATEVIEW
     url(r'^Update/(?P<pk>\d+)/$', EliminarPais.as_view(), name= 'Update'),

     #CRUD: DeleteView
     url(r'^Delete/(?P<pk>\d+)/$', ActualizarPais.as_view(), name= 'Delete'),
     
#    url(r'^$', Paises_Africanos1.as_view(), name= 'Paises_Africanos1'),
#    url(r'^Paises_Africanos2/$',Paises_Africanos2.as_view(),name= 'Paises_Africanos2'),
#    url(r'^Paises_Africanos3/$', Paises_Africanos3.as_view(), name= 'Paises_Africanos3'),
#    url(r'^Paises_Africanos4/$',  Paises_Africanos4.as_view(),    name= 'Paises_Africanos4'),
#    url(r'^Paises_Africanos5/$', Paises_Africanos5.as_view(), name= 'Paises_Africanos5'),
#    url(r'^Paises_Africanos6/$', Paises_Africanos6.as_view(), name= 'Paises_Africanos6'),
#    url(r'^Paises_Africanos7/$', Paises_Africanos7.as_view(), name= 'Paises_Africanos7'),
#    url(r'^Paises_Africanos8/$',Paises_Africanos8.as_view(), name= 'Paises_Africanos8'),
#    url(r'^Paises_Africanos9/$', Paises_Africanos9.as_view(), name= 'Paises_Africanos9'),
#    url(r'^Paises_Africanos10/$', Paises_Africanos10.as_view(), name= 'Paises_Africanos10'),
]
