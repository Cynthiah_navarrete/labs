from django import forms
from .models import Paises_Africanos

# Cliente
class PaisForm(forms.ModelForm):
    class Meta:
        model = Paises_Africanos
        fields = [
            'id',
            'Pais',
            'Capital',
            'Territorio',
            'Poblacion',
        ]
        labels = {
                'id': 'Identificador',
                'Pais': 'Pais',
                'Capital':'  Capital',
                'Territorio':' Territorio ',
                'Poblacion':' Poblacion',
        }
