# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import PaisForm
from .models import Paises_Africanos

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarPais(CreateView):
    model = Paises_Africanos
    form_class = PaisForm
    template_name = 'Project10/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarPais( ListView):
    model = Paises_Africanos
    template_name = 'Project10/Retrieve.html'
    context_object_name = 'pais'
    queryset = Paises_Africanos.objects.all()


#DETAILEVIEW
class DetailPais( DetailView):
    model = Paises_Africanos
    template_name = 'Project10/Detail.html'
    context_object_name = 'pais'


#UPDATEVIEW
class ActualizarPais( UpdateView):
    model = Paises_Africanos
    form_class = PaisForm
    template_name = 'Project10/Update.html'
    context_object_name = 'pais'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarPais( DeleteView):
    model = Paises_Africanos
    template_name = "Project10/Delete.html"
    context_object_name = "pais"
    success_url = reverse_lazy('Retrieve')



# Create your views here.

class Paises_Africanos1(TemplateView):
    template_name = 'Project10/1.html'

class Paises_Africanos2(TemplateView):
    template_name = 'Project10/2.html'

class Paises_Africanos3(TemplateView):
    template_name = 'Project10/3.html'

class Paises_Africanos4(TemplateView):
    template_name = 'Project10/4.html'

class Paises_Africanos5(TemplateView):
    template_name = 'Project10/5.html'

class Paises_Africanos6(TemplateView):
    template_name = 'Project10/6.html'

class Paises_Africanos7(TemplateView):
    template_name = 'Project10/7.html'

class Paises_Africanos8(TemplateView):
    template_name = 'Project10/8.html'

class Paises_Africanos9(TemplateView):
    template_name = 'Project10/9.html'

class Paises_Africanos10(TemplateView):
    template_name = 'Project10/10.html'
