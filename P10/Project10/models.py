# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 10
#MODEL : PAISES AFRICANOS

class Paises_Africanos(models.Model):
    id = models.AutoField(primary_key = True)
    Pais = models.CharField(max_length = 50)
    Capital= models.CharField(max_length = 50)
    Territorio = models.IntegerField()
    Poblacion = models.IntegerField()


    def __str__(self):
        return self.Pais
