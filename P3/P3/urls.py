"""P3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project3 import views
from Project3.views  import EliminarLibro, EditarLibro, DetailLibro, AgregarLibro, ListarLibro
#import Libro1, Libro2, Libro3, Libro4, Libro5, Libro6, Libro7, Libro8, Libro9, Libro10

urlpatterns = [

    url(r'^admin/', admin.site.urls),

    #CRUD: RETRIEVEVIEW
    url(r'^$', ListarLibro.as_view(), name= 'Retrieve'),

    #CRUD: CREATEVIEW
    url(r'^Create/$', AgregarLibro.as_view(), name= 'Create'),

    #CRUD: DETAILEVIEW
    url(r'^Detail/(?P<pk>\d+)/$', DetailLibro.as_view(), name= 'Detail'),

    #CRUD:UPDATEVIEW
    url(r'^Update/(?P<pk>\d+)/$', EditarLibro.as_view(), name= 'Update'),

    #CRUD: DeleteView
    url(r'^Delete/(?P<pk>\d+)/$', EliminarLibro.as_view(), name= 'Delete'),

#    url(r'^admin/', admin.site.urls),
#    url(r'^$', Libro1.as_view(), name= 'Libro1'),
#    url(r'^Libro2/$',Libro2.as_view(),name= 'Libro2'),
#    url(r'^Libro3/$', Libro3.as_view(), name= 'Libro3'),
#    url(r'^Libro4/$',  Libro4.as_view(),    name= 'Libro4'),
#    url(r'^Libro5/$', Libro5.as_view(), name= 'Libro5'),
#    url(r'^Libro6/$', Libro6.as_view(), name= 'Libro6'),
#    url(r'^Libro7/$', Libro7.as_view(), name= 'Libro7'),
#    url(r'^Libro9/$', Libro9.as_view(), name= 'Libro9'),
#    url(r'^Libro10/$', Libro10.as_view(), name= 'Libro10'),
]
