# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 3
#MODEL : LIBROS

class Libro(models.Model):
    id = models.AutoField(primary_key = True)
    Titulo_del_Libro = models.CharField(max_length = 50)
    Fecha_de_creacion = models.IntegerField()
    Autor = models.CharField(max_length = 50)
    Genero = models.CharField(max_length = 20)


    def __str__(self):
        return self.Titulo_del_Libro
