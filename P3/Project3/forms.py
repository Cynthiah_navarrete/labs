from django import forms
from .models import Libro

# Cliente
class LibroForm(forms.ModelForm):
    class Meta:
        model = Libro
        fields = [
            'id',
            'Titulo_del_Libro',
            'Fecha_de_creacion',
            'Autor',
            'Genero',
        ]
        labels = {
                'id':'id',
                'Titulo_del_Libro':'Titulo del Libro',
                'Fecha_de_creacion':'Fecha de creacion',
                'Autor':'Autor',
                'Genero':'Genero',
        }
