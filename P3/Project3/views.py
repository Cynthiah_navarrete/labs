# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import LibroForm
from .models import Libro

################ CRUDE: CLIENTE ################
#EliminarLibro, EditarLibro, DetailLibro, AgregarLibro, ListarLibro

#CREATEVIEW
class AgregarLibro(CreateView):
    model = Libro
    form_class = LibroForm
    template_name = 'Project3/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarLibro( ListView):
    model = Libro
    template_name = 'Project3/Retrieve.html'
    context_object_name = 'libros'
    queryset = Libro.objects.all()


#DETAILEVIEW
class DetailLibro( DetailView):
    model = Libro
    template_name = 'Project3/Detail.html'
    context_object_name = 'libros'


#UPDATEVIEW
class EditarLibro( UpdateView):
    model = Libro
    form_class = LibroForm
    template_name = 'Project3/Update.html'
    context_object_name = 'libros'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarLibro( DeleteView):
    model = Libro
    template_name = "Project3/Delete.html"
    context_object_name = "libros"
    success_url = reverse_lazy('Retrieve')




class Libro1(TemplateView):
    template_name = 'Project3/1.html'

class Libro2(TemplateView):
    template_name = 'Project3/2.html'

class Libro3(TemplateView):
    template_name = 'Project3/3.html'

class Libro4(TemplateView):
    template_name = 'Project3/4.html'

class Libro5(TemplateView):
    template_name = 'Project3/5.html'

class Libro6(TemplateView):
    template_name = 'Project3/6.html'

class Libro7(TemplateView):
    template_name = 'Project3/7.html'

class Libro8(TemplateView):
    template_name = 'Project3/8.html'

class Libro9(TemplateView):
    template_name = 'Project3/9.html'

class Libro10(TemplateView):
    template_name = 'Project3/10.html'
