"""P7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project7 import views
from Project7.views import   ListarPais, AgregarPais, DetailPais,  EliminarPais, ActualizarPais
#import Paises_Americanos1, Paises_Americanos2, Paises_Americanos3, Paises_Americanos4, Paises_Americanos5, Paises_Americanos6, Paises_Americanos7, Paises_Americanos8, Paises_Americanos9, Paises_Americanos10

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #CRUD: RETRIEVEVIEW
    url(r'^$', ListarPais.as_view(), name= 'Retrieve'),

    #CRUD: CREATEVIEW
    url(r'^Create/$', AgregarPais.as_view(), name= 'Create'),

    #CRUD: DETAILEVIEW
    url(r'^Detail/(?P<pk>\d+)/$', DetailPais.as_view(), name= 'Detail'),

    #CRUD:UPDATEVIEW
    url(r'^Update/(?P<pk>\d+)/$', EliminarPais.as_view(), name= 'Update'),

    #CRUD: DeleteView
    url(r'^Delete/(?P<pk>\d+)/$', ActualizarPais.as_view(), name= 'Delete'),



#    url(r'^$', Paises_Americanos1.as_view(), name= 'Paises_Americanos1'),
#    url(r'^Paises_Americanos2/$',Paises_Americanos2.as_view(),name= 'Paises_Americanos2'),
#    url(r'^Paises_Americanos3/$', Paises_Americanos3.as_view(), name= 'Paises_Americanos3'),
#    url(r'^Paises_Americanos4/$',  Paises_Americanos4.as_view(),    name= 'Paises_Americanos4'),
#    url(r'^Paises_Americanos5/$', Paises_Americanos5.as_view(), name= 'Paises_Americanos5'),
#    url(r'^Paises_Americanos6/$', Paises_Americanos6.as_view(), name= 'Paises_Americanos6'),
#    url(r'^Paises_Americanos7/$', Paises_Americanos7.as_view(), name= 'Paises_Americanos7'),
#    url(r'^Paises_Americanos8/$',Paises_Americanos8.as_view(), name= 'Paises_Americanos8'),
#    url(r'^Paises_Americanos9/$', Paises_Americanos9.as_view(), name= 'Paises_Americanos9'),
#    url(r'^Paises_Americanos10/$', Paises_Americanos10.as_view(), name= 'Paises_Americanos10'),
]
