# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-07-01 02:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Project7', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paises_americanos',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
