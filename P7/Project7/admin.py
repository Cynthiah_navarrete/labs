# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from .models import Paises_Americanos

admin.site.register(Paises_Americanos)
