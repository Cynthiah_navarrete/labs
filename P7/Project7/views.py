# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import PaisForm
from .models import Paises_Americanos

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarPais(CreateView):
    model = Paises_Americanos
    form_class = PaisForm
    template_name = 'Project7/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarPais( ListView):
    model = Paises_Americanos
    template_name = 'Project7/Retrieve.html'
    context_object_name = 'pais'
    queryset = Paises_Americanos.objects.all()


#DETAILEVIEW
class DetailPais( DetailView):
    model = Paises_Americanos
    template_name = 'Project7/Detail.html'
    context_object_name = 'pais'


#UPDATEVIEW
class ActualizarPais( UpdateView):
    model = Paises_Americanos
    form_class = PaisForm
    template_name = 'Project7/Update.html'
    context_object_name = 'pais'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarPais( DeleteView):
    model = Paises_Americanos
    template_name = "Project7/Delete.html"
    context_object_name = "pais"
    success_url = reverse_lazy('Retrieve')


class Paises_Americanos1(TemplateView):
    template_name = 'Project7/1.html'

class Paises_Americanos2(TemplateView):
    template_name = 'Project7/2.html'

class Paises_Americanos3(TemplateView):
    template_name = 'Project7/3.html'

class Paises_Americanos4(TemplateView):
    template_name = 'Project7/4.html'

class Paises_Americanos5(TemplateView):
    template_name = 'Project7/5.html'

class Paises_Americanos6(TemplateView):
    template_name = 'Project7/6.html'

class Paises_Americanos7(TemplateView):
    template_name = 'Project7/7.html'

class Paises_Americanos8(TemplateView):
    template_name = 'Project7/8.html'

class Paises_Americanos9(TemplateView):
    template_name = 'Project7/9.html'

class Paises_Americanos10(TemplateView):
    template_name = 'Project7/10.html'
