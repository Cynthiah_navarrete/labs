# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 5
#MODEL : AUTOS

class Auto(models.Model):
    id = models.AutoField(primary_key = True)
    Modelo = models.CharField(max_length = 50)
    Marca = models.CharField(max_length = 50)
    Anio = models.CharField(max_length = 50)


    def __str__(self):
        return self.Modelo
