from django import forms
from .models import Auto

# Cliente
class AutoForm(forms.ModelForm):
    class Meta:
        model = Auto
        fields = [
            'id',
            'Modelo',
            'Marca',
            'Anio',
        ]
        labels = {
                'id':'id',
                'Modelo':'  Modelo ',
                'Marca':'Marca',
                'Anio':'Anio',
        }
