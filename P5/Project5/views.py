# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import AutoForm
from .models import Auto


################ CRUDE: CLIENTE ################
#ListarAuto, AgregarAuto, DetailAuto, EditarAuto,EliminarAuto
#CREATEVIEW
class ListarAuto(CreateView):
    model = Auto
    form_class = AutoForm
    template_name = 'Project5/Retrive.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class AgregarAuto( ListView):
    model = Auto
    template_name = 'Project5/Retrieve.html'
    context_object_name = 'autos'
    queryset = Auto.objects.all()


#DETAILEVIEW
class DetailAuto( DetailView):
    model = Auto
    template_name = 'Project5/Detail.html'
    context_object_name = 'autos'


#UPDATEVIEW
class EditarAuto( UpdateView):
    model = Auto
    form_class = AutoForm
    template_name = 'Project5/Update.html'
    context_object_name = 'autos'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarAuto( DeleteView):
    model = Auto
    template_name = "Project5/Delete.html"
    context_object_name = "autos"
    success_url = reverse_lazy('Retrieve')


# Create your views here.

class Auto1(TemplateView):
    template_name = 'Project5/1.html'

class Auto2(TemplateView):
    template_name = 'Project5/2.html'

class Auto3(TemplateView):
    template_name = 'Project5/3.html'

class Auto4(TemplateView):
    template_name = 'Project5/4.html'

class Auto5(TemplateView):
    template_name = 'Project5/5.html'

class Auto6(TemplateView):
    template_name = 'Project5/6.html'

class Auto7(TemplateView):
    template_name = 'Project5/7.html'

class Auto8(TemplateView):
    template_name = 'Project5/8.html'

class Auto9(TemplateView):
    template_name = 'Project5/9.html'

class Auto10(TemplateView):
    template_name = 'Project5/10.html'
