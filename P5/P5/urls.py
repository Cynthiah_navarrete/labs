"""P5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project5 import views
from Project5.views import  ListarAuto, AgregarAuto, DetailAuto, EditarAuto,EliminarAuto

urlpatterns = [
    url(r'^admin/', admin.site.urls),

        #CRUD: RETRIEVEVIEW
        url(r'^Retrive/$', ListarAuto.as_view(), name= 'Retrieve'),

        #CRUD: CREATEVIEW
        url(r'^Create/$', AgregarAuto.as_view(), name= 'Create'),

        #CRUD: DETAILEVIEW
        url(r'^Detail/(?P<pk>\d+)/$', DetailAuto.as_view(), name= 'Detail'),

        #CRUD:UPDATEVIEW
        url(r'^Update/(?P<pk>\d+)/$', EditarAuto.as_view(), name= 'Update'),

        #CRUD: DeleteView
        url(r'^Delete/(?P<pk>\d+)/$', EliminarAuto.as_view(), name= 'Delete'),

    #url(r'^$', Auto1.as_view(), name= 'Auto1'),
    #url(r'^Auto2/$',Auto2.as_view(),name= 'Auto2'),
    #url(r'^Auto3/$', Auto3.as_view(), name= 'Auto3'),
    #url(r'^Auto4/$',  Auto4.as_view(),    name= 'Auto4'),
    #url(r'^Auto5/$', Auto5.as_view(), name= 'Auto5'),
    #url(r'^Auto6/$', Auto6.as_view(), name= 'Auto6'),
    #url(r'^Auto7/$', Auto7.as_view(), name= 'Auto7'),
    #url(r'^Auto8/$',Auto8.as_view(), name= 'Auto8'),
    #url(r'^Auto9/$', Auto9.as_view(), name= 'Auto9'),
    #url(r'^Auto10/$', Auto10.as_view(), name= 'Auto10'),
]
