"""P2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project2 import views
from Project2.views import EliminarPinturas, EditarPinturas, DetailPinturas, AgregarPinturas, ListarPinturas
#views import Pintura1,Pintura2,Pintura3,Pintura4,Pintura5,Pintura6,Pintura7,Pintura8,Pintura9, Pintura10

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #CRUD: RETRIEVEVIEW
    url(r'^$', ListarPinturas.as_view(), name= 'Retrieve'),

    #CRUD: CREATEVIEW
    url(r'^Create/$', AgregarPinturas.as_view(), name= 'Create'),

    #CRUD: DETAILEVIEW
    url(r'^Detail/(?P<pk>\d+)/$', DetailPinturas.as_view(), name= 'Detail'),

    #CRUD:UPDATEVIEW
    url(r'^Update/(?P<pk>\d+)/$', EditarPinturas.as_view(), name= 'Update'),

    #CRUD: DeleteView
    url(r'^Delete/(?P<pk>\d+)/$', EliminarPinturas.as_view(), name= 'Delete'),

#    url(r'^$', Pintura1.as_view(), name= 'Pintura1'),
#    url(r'^Pintura2/$',Pintura2.as_view(),name= 'Pintura2'),
#    url(r'^Pintura3/$', Pintura3.as_view(), name= 'Pintura3'),
#    url(r'^Pintura4/$',  Pintura4.as_view(),    name= 'Pintura4'),
#    url(r'^Pintura5/$', Pintura5.as_view(), name= 'Pintura5'),
#    url(r'^Pintura7/$', Pintura7.as_view(), name= 'Pintura7'),
#    url(r'^Pintura8/$',Pintura8.as_view(), name= 'Pintura8'),
#    url(r'^Pintura9/$', Pintura9.as_view(), name= 'Pintura9'),
#    url(r'^Pintura10/$', Pintura10.as_view(), name= 'Pintura10'),
]
