# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-07-01 00:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Project2', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pinturas',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
