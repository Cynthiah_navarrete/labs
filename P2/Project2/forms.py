from django import forms
from .models import Pinturas

# Cliente
class PinturaForm(forms.ModelForm):
    class Meta:
        model = Pinturas
        fields = [
            'id',
            'Nombre_de_la_pintura',
            'Fecha_de_creacion',
            'Autor',
            'Estilo',
        ]
        labels = {
                'id':'id',
                'Nombre_de_la_pintura':'Nombre_de_la_pintura',
                'Fecha_de_creacion':'Fecha_de_creacion',
                'Autor':'Autor',
                'Estilo':'Estilo',
        }
