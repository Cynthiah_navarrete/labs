# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import PinturaForm
from .models import Pinturas

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarPinturas(CreateView):
    model = Pinturas
    form_class = PinturaForm
    template_name = 'Project2/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarPinturas( ListView):
    model = Pinturas
    template_name = 'Project2/Retrieve.html'
    context_object_name = 'pinturas'
    queryset = Pinturas.objects.all()


#DETAILEVIEW
class DetailPinturas( DetailView):
    model = Pinturas
    template_name = 'Project2/Detail.html'
    context_object_name = 'pinturas'


#UPDATEVIEW
class EditarPinturas( UpdateView):
    model = Pinturas
    form_class = PinturaForm
    template_name = 'Project2/Update.html'
    context_object_name = 'pinturas'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarPinturas( DeleteView):
    model = Pinturas
    template_name = "Project2/Delete.html"
    context_object_name = "pinturas"
    success_url = reverse_lazy('Retrieve')



# Create your views here.

class Pintura1(TemplateView):
    template_name = 'Project2/1.html'

class Pintura2(TemplateView):
    template_name = 'Project2/2.html'

class Pintura3(TemplateView):
    template_name = 'Project2/3.html'

class Pintura4(TemplateView):
    template_name = 'Project2/4.html'

class Pintura5(TemplateView):
    template_name = 'Project2/5.html'

class Pintura6(TemplateView):
    template_name = 'Project2/6.html'

class Pintura7(TemplateView):
    template_name = 'Project2/7.html'

class Pintura8(TemplateView):
    template_name = 'Project2/8.html'

class Pintura9(TemplateView):
    template_name = 'Project2/9.html'

class Pintura10(TemplateView):
    template_name = 'Project2/10.html'
