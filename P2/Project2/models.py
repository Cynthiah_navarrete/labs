# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 2
#MODEL : PINTURAS

class Pinturas(models.Model):
    id = models.AutoField(primary_key = True)
    Nombre_de_la_pintura = models.CharField(max_length = 50)
    Fecha_de_creacion = models.IntegerField()
    Autor = models.CharField(max_length = 50)
    Estilo = models.CharField(max_length = 20)


    def __str__(self):
        return self.Nombre_de_la_pintura
