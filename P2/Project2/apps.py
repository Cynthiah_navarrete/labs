# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Project2Config(AppConfig):
    name = 'Project2'
