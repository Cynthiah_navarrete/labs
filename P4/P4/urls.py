"""P4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project4 import views
from Project4.views import ListarAnimal, AgregarAnimal, DetailAnimal, EditarAnimal, EliminarAnimal

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #CRUD: RETRIEVEVIEW
    url(r'^$', ListarAnimal.as_view(), name= 'Retrieve'),

    #CRUD: CREATEVIEW
    url(r'^Create/$', AgregarAnimal.as_view(), name= 'Create'),

    #CRUD: DETAILEVIEW
    url(r'^Detail/(?P<pk>\d+)/$', DetailAnimal.as_view(), name= 'Detail'),

    #CRUD:UPDATEVIEW
    url(r'^Update/(?P<pk>\d+)/$', EditarAnimal.as_view(), name= 'Update'),

    #CRUD: DeleteView
    url(r'^Delete/(?P<pk>\d+)/$', EliminarAnimal.as_view(), name= 'Delete'),


#    url(r'^$', Animal1.as_view(), name= 'Animal1'),
#    url(r'^Animal2/$',Animal2.as_view(),name= 'Animal2'),
#    url(r'^Animal3/$', Animal3.as_view(), name= 'Animal3'),
#    url(r'^Animal4/$',  Animal4.as_view(),    name= 'Animal4'),
#    url(r'^Animal5/$', Animal5.as_view(), name= 'Animal5'),
#    url(r'^Animal6/$', Animal6.as_view(), name= 'Animal6'),
#    url(r'^Animal7/$', Animal7.as_view(), name= 'Animal7'),
#    url(r'^Animal8/$',Animal8.as_view(), name= 'Animal8'),
#    url(r'^Animal9/$', Animal9.as_view(), name= 'Animal9'),
#    url(r'^Animal10/$', Animal10.as_view(), name= 'Animal10'),
]
