# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import AnimalForm
from .models import Animal

# Create your views here.

################ CRUDE: CLIENTE ################
#CREATEVIEW
class AgregarAnimal(CreateView):
    model = Animal
    form_class = AnimalForm
    template_name = 'Project4/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarAnimal( ListView):
    model = Animal
    template_name = 'Project4/Retrieve.html'
    context_object_name = 'animales'
    queryset = Animal.objects.all()


#DETAILEVIEW
class DetailAnimal( DetailView):
    model = Animal
    template_name = 'Project4/Detail.html'
    context_object_name = 'animales'


#UPDATEVIEW
class EditarAnimal( UpdateView):
    model = Animal
    form_class = AnimalForm
    template_name = 'Project4/Update.html'
    context_object_name = 'animales'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarAnimal( DeleteView):
    model = Animal
    template_name = "Project4/Delete.html"
    context_object_name = "animales"
    success_url = reverse_lazy('Retrieve')


# Create your views here.

class Animal1(TemplateView):
    template_name = 'Project4/1.html'

class Animal2(TemplateView):
    template_name = 'Project4/2.html'

class Animal3(TemplateView):
    template_name = 'Project4/3.html'

class Animal4(TemplateView):
    template_name = 'Project4/4.html'

class Animal5(TemplateView):
    template_name = 'Project4/5.html'

class Animal6(TemplateView):
    template_name = 'Project4/6.html'

class Animal7(TemplateView):
    template_name = 'Project4/7.html'

class Animal8(TemplateView):
    template_name = 'Project4/8.html'

class Animal9(TemplateView):
    template_name = 'Project4/9.html'

class Animal10(TemplateView):
    template_name = 'Project4/10.html'
