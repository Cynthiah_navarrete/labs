# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# PROJECT 4
#MODEL : ANIMALES

class Animal(models.Model):
    id = models.AutoField(primary_key = True)
    Nombre_Comun = models.CharField(max_length = 50)
    Nombre_Cientifico = models.CharField(max_length = 50)
    Especie = models.CharField(max_length = 50)
    Alimentacion = models.CharField(max_length = 20)


    def __str__(self):
        return self.Nombre_Comun
