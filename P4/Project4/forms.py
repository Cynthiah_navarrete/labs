from django import forms
from .models import Animal

# Cliente
class AnimalForm(forms.ModelForm):
    class Meta:
        model = Animal
        fields = [
            'id',
            'Nombre_Comun',
            'Nombre_Cientifico',
            'Especie',
            'Alimentacion',
        ]
        labels = {
                'id':'id',
                'Nombre_Comun':'  Nombre ',
                'Nombre_Cientifico':'Nombre_Cientifico',
                'Especie':'Especie',
                'Alimentacion':'Alimentacion',
        }
