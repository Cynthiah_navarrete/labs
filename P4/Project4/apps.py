# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Project4Config(AppConfig):
    name = 'Project4'
