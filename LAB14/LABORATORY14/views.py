# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import timezone
from .models import Publicacion
from .forms import PublicacionForm
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404


# Create your views here.

def publicacion_list(request):

    publicaciones = Publicacion.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'LABORATORY14/publicacion_list.html', {'publicaciones': publicaciones})


def publicacion_nueva(request):

    if request.method == "POST":
        form = PublicacionForm(request.POST)
        if form.is_valid():

                Publicacion = form.save(commit=False)
                Publicacion.published_date = timezone.now()
                post.save()
                return redirect('publicacion_nueva', pk=Publicacion.pk)
    else:
        form= PublicacionForm()
    return render(request, 'LABORATORY14/publicacion_nueva.html', {'form': form})


def publicacion_detail(request, pk):
    publicaciones = get_object_or_404(Publicacion, pk=pk)
    return render(request, 'LABORATORY14/publicacion_detail.html', {'publicaciones': publicaciones})
