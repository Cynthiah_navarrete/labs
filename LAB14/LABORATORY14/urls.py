

from django.conf.urls import url
from . import views


urlpatterns = [

  url('', views.publicacion_list, name='publicacion_list'),
 #url('', views.publicacion_nueva, name='publicacion_nueva'),
  url('publicacion/<int:pk>/', views.publicacion_detail, name='publicacion_detail'),
    ]
