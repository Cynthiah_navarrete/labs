# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Project9Config(AppConfig):
    name = 'Project9'
