# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import PaisForm
from .models import Paises_Asiaticos

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarPais(CreateView):
    model = Paises_Asiaticos
    form_class = PaisForm
    template_name = 'Project9/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarPais( ListView):
    model = Paises_Asiaticos
    template_name = 'Project9/Retrieve.html'
    context_object_name = 'pais'
    queryset = Paises_Asiaticos.objects.all()


#DETAILEVIEW
class DetailPais( DetailView):
    model = Paises_Asiaticos
    template_name = 'Project9/Detail.html'
    context_object_name = 'pais'


#UPDATEVIEW
class ActualizarPais( UpdateView):
    model = Paises_Asiaticos
    form_class = PaisForm
    template_name = 'Project9/Update.html'
    context_object_name = 'pais'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarPais( DeleteView):
    model = Paises_Asiaticos
    template_name = "Project9/Delete.html"
    context_object_name = "pais"
    success_url = reverse_lazy('Retrieve')



# Create your views here.

class Paises_Asiaticos1(TemplateView):
    template_name = 'Project9/1.html'

class Paises_Asiaticos2(TemplateView):
    template_name = 'Project9/2.html'

class Paises_Asiaticos3(TemplateView):
    template_name = 'Project9/3.html'

class Paises_Asiaticos4(TemplateView):
    template_name = 'Project9/4.html'

class Paises_Asiaticos5(TemplateView):
    template_name = 'Project9/5.html'

class Paises_Asiaticos6(TemplateView):
    template_name = 'Project9/6.html'

class Paises_Asiaticos7(TemplateView):
    template_name = 'Project9/7.html'

class Paises_Asiaticos8(TemplateView):
    template_name = 'Project9/8.html'

class Paises_Asiaticos9(TemplateView):
    template_name = 'Project9/9.html'

class Paises_Asiaticos10(TemplateView):
    template_name = 'Project9/10.html'
