from django import forms
from .models import Paises_Asiaticos

# Cliente
class PaisForm(forms.ModelForm):
    class Meta:
        model = Paises_Asiaticos
        fields = [
            'id',
            'Pais',
            'Capital',
            'Territorio',
            'Poblacion',
        ]
        labels = {
                'id': 'Identificador',
                'Pais': 'Pais',
                'Capital':'  Capital',
                'Territorio':' Territorio ',
                'Poblacion':' Poblacion',
        }
