"""P9 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project9 import views
from Project9.views import ListarPais, AgregarPais, DetailPais,  EliminarPais, ActualizarPais
#Paises_Asiaticos1, Paises_Asiaticos2, Paises_Asiaticos3, Paises_Asiaticos4, Paises_Asiaticos5, Paises_Asiaticos6, Paises_Asiaticos7, Paises_Asiaticos8, Paises_Asiaticos9, Paises_Asiaticos10

urlpatterns = [
   url(r'^admin/', admin.site.urls),

   #CRUD: RETRIEVEVIEW
   url(r'^$', ListarPais.as_view(), name= 'Retrieve'),

   #CRUD: CREATEVIEW
   url(r'^Create/$', AgregarPais.as_view(), name= 'Create'),

   #CRUD: DETAILEVIEW
   url(r'^Detail/(?P<pk>\d+)/$', DetailPais.as_view(), name= 'Detail'),

   #CRUD:UPDATEVIEW
   url(r'^Update/(?P<pk>\d+)/$', EliminarPais.as_view(), name= 'Update'),

   #CRUD: DeleteView
   url(r'^Delete/(?P<pk>\d+)/$', ActualizarPais.as_view(), name= 'Delete'),


#    url(r'^$', Paises_Asiaticos1.as_view(), name= 'Paises_Asiaticos1'),
#    url(r'^Paises_Asiaticos2/$',Paises_Asiaticos2.as_view(),name= 'Paises_Asiaticos2'),
#    url(r'^Paises_Asiaticos3/$', Paises_Asiaticos3.as_view(), name= 'Paises_Asiaticos3'),
#    url(r'^Paises_Asiaticos4/$',  Paises_Asiaticos4.as_view(),    name= 'Paises_Asiaticos4'),
#    url(r'^Paises_Asiaticos5/$', Paises_Asiaticos5.as_view(), name= 'Paises_Asiaticos5'),
#    url(r'^Paises_Asiaticos6/$', Paises_Asiaticos6.as_view(), name= 'Paises_Asiaticos6'),
#    url(r'^Paises_Asiaticos7/$', Paises_Asiaticos7.as_view(), name= 'Paises_Asiaticos7'),
#    url(r'^Paises_Asiaticos8/$',Paises_Asiaticos8.as_view(), name= 'Paises_Asiaticos8'),
#    url(r'^Paises_Asiaticos9/$', Paises_Asiaticos9.as_view(), name= 'Paises_Asiaticos9'),
#    url(r'^Paises_Asiaticos10/$', Paises_Asiaticos10.as_view(), name= 'Paises_Asiaticos10'),
]
