"""P8 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from Project8 import views
from Project8.views import  ListarPais, AgregarPais, DetailPais,  EliminarPais, ActualizarPais
#Paises_Europeos1,Paises_Europeos2 , Paises_Europeos3, Paises_Europeos4, Paises_Europeos5, Paises_Europeos6, Paises_Europeos7, Paises_Europeos8, Paises_Europeos9, Paises_Europeos10

urlpatterns = [

    url(r'^admin/', admin.site.urls),

        #CRUD: RETRIEVEVIEW
        url(r'^$', ListarPais.as_view(), name= 'Retrieve'),

        #CRUD: CREATEVIEW
        url(r'^Create/$', AgregarPais.as_view(), name= 'Create'),

        #CRUD: DETAILEVIEW
        url(r'^Detail/(?P<pk>\d+)/$', DetailPais.as_view(), name= 'Detail'),

        #CRUD:UPDATEVIEW
        url(r'^Update/(?P<pk>\d+)/$', EliminarPais.as_view(), name= 'Update'),

        #CRUD: DeleteView
        url(r'^Delete/(?P<pk>\d+)/$', ActualizarPais.as_view(), name= 'Delete'),





    #url(r'^$', Paises_Europeos1.as_view(), name= 'Paises_Europeos1'),
    #url(r'^Paises_Europeos2/$',Paises_Europeos2.as_view(),name= 'Paises_Europeos2'),
    #url(r'^Paises_Europeos3/$', Paises_Europeos3.as_view(), name= 'Paises_Europeos3'),
    #url(r'^Paises_Europeos4/$',  Paises_Europeos4.as_view(),    name= 'Paises_Europeos4'),
    #url(r'^Paises_Europeos5/$', Paises_Europeos5.as_view(), name= 'Paises_Europeos5'),
    #url(r'^Paises_Europeos6/$', Paises_Europeos6.as_view(), name= 'Paises_Europeos6'),
    #url(r'^Paises_Europeos7/$', Paises_Europeos7.as_view(), name= 'Paises_Europeos7'),
    #url(r'^Paises_Europeos8/$',Paises_Europeos8.as_view(), name= 'Paises_Europeos8'),
    #url(r'^Paises_Europeos9/$', Paises_Europeos9.as_view(), name= 'Paises_Europeos9'),
    #url(r'^Paises_Europeos10/$', Paises_Europeos10.as_view(), name= 'Paises_Europeos10'),
]
