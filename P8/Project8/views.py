# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView,CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import PaisForm
from .models import Paises_Europeos

################ CRUDE: CLIENTE ################

#CREATEVIEW
class AgregarPais(CreateView):
    model = Paises_Europeos
    form_class = PaisForm
    template_name = 'Project8/Create.html'
    success_url = reverse_lazy('Retrieve')


#RETRIEVEVIEW
class ListarPais( ListView):
    model = Paises_Europeos
    template_name = 'Project8/Retrieve.html'
    context_object_name = 'pais'
    queryset = Paises_Europeos.objects.all()


#DETAILEVIEW
class DetailPais( DetailView):
    model = Paises_Europeos
    template_name = 'Project8/Detail.html'
    context_object_name = 'pais'


#UPDATEVIEW
class ActualizarPais( UpdateView):
    model = Paises_Europeos
    form_class = PaisForm
    template_name = 'Project8/Update.html'
    context_object_name = 'pais'
    success_url = reverse_lazy('Retrieve')

#DELETEVIEW
class EliminarPais( DeleteView):
    model = Paises_Europeos
    template_name = "Project8/Delete.html"
    context_object_name = "pais"
    success_url = reverse_lazy('Retrieve')


# Create your views here.

class Paises_Europeos1(TemplateView):
    template_name = 'Project8/1.html'

class Paises_Europeos2(TemplateView):
    template_name = 'Project8/2.html'

class Paises_Europeos3(TemplateView):
    template_name = 'Project8/3.html'

class Paises_Europeos4(TemplateView):
    template_name = 'Project8/4.html'

class Paises_Europeos5(TemplateView):
    template_name = 'Project8/5.html'

class Paises_Europeos6(TemplateView):
    template_name = 'Project8/6.html'

class Paises_Europeos7(TemplateView):
    template_name = 'Project8/7.html'

class Paises_Europeos8(TemplateView):
    template_name = 'Project8/8.html'

class Paises_Europeos9(TemplateView):
    template_name = 'Project8/9.html'

class Paises_Europeos10(TemplateView):
    template_name = 'Project8/10.html'
