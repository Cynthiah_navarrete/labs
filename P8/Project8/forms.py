from django import forms
from .models import Paises_Europeos

# Cliente
class PaisForm(forms.ModelForm):
    class Meta:
        model = Paises_Europeos
        fields = [
            'id',
            'Pais',
            'Capital',
            'Territorio',
            'Poblacion',
        ]
        labels = {
                'id': 'Identificador',
                'Pais': 'Pais',
                'Capital':'  Capital',
                'Territorio':' Territorio ',
                'Poblacion':' Poblacion',
        }
